<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Imp Management Inventory</title>
</head>
<body>
	<div align="center">
		<h1>Employee List</h1>
		<h3>
			<a href="newAsset">New Asset</a>
		</h3>
		<table border="1">
		
			<tr>
				<th>id</th>
				<th>Asset Type</th>
				<th>Asset Make</th>
				<th>Asset Model</th>
				<th>Serial Number</th>
				<th>Date Of Purchase</th>
				<th>Supplier</th>
				<th>Comments</th>
				<th>Life Span</th>
			</tr>

			<c:forEach var="asset" items="${listAsset}">
				<tr>

					<td>${asset.id}</td>
					<td>${asset.type}</td>
					<td>${asset.make}</td>
					<td>${asset.model}</td>
					<td>${asset.serialNumber}</td>
					<td>${asset.dateOfPurchase}</td>
					<td>${asset.supplier}</td>
					<td>${asset.comments}</td>
					<td>${asset.lifeSpan}</td>
					<td><a href="editAsset?id=${asset.id}">Edit</a> <a
						href="deleteAsset?id=${asset.id}">Delete</a></td>

				</tr>
			</c:forEach>
			
		</table>
	</div>
</body>
</html>