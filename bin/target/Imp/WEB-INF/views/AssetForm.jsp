<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Add/Edit Asset</title>
</head>
<body>
	<div align="center">
		<h1>Add/Edit Asset</h1>
		<form:form action="saveAsset" method="post" modelAttribute="asset">
			<table>
				<form:hidden path="id" />
				<tr>
					<td>Type:</td>
					<td><form:input path="type" /></td>
				</tr>
				<tr>
					<td>Make:</td>
					<td><form:input path="make" /></td>
				</tr>
				<tr>
					<td>Model:</td>
					<td><form:input path="model" /></td>
				</tr>
				<tr>
					<td>Serial Number:</td>
					<td><form:input path="serialNumber" /></td>
				</tr>
				<tr>
					<td>Date Of Purchase:</td>
					<td><form:input path="dateOfPurchase" /></td>
				</tr>
				<tr>
					<td>Supplier:</td>
					<td><form:input path="supplier" /></td>
				</tr>
				<tr>
					<td>Comments:</td>
					<td><form:input path="comments" /></td>
				</tr>
				<tr>
					<td>Life Span:</td>
					<td><form:input path="lifeSpan" /></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><input type="submit" value="Save"></td>
				</tr>
			</table>
		</form:form>
	</div>
</body>
</html>