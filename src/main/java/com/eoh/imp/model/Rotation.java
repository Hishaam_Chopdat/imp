package com.eoh.imp.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "Rotation")
public class Rotation {

	@Id
	@Column(name = "rotid")
	private int id;

	@Column(name = "type")
	private String type;

	@Column(name = "make")
	private String make;

	@Column(name = "model")
	private String model;

	@Column(name = "serialnumber")
	private String serialNumber;

	@Column(name = "designation")
	private String designation;
	
	@Column(name = "busunit")
	private String busUnit;
	
	@Column(name = "rotation")
	private String rotation;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}


	public String getBusUnit() {
		return busUnit;
	}

	public void setBusUnit(String busUnit) {
		this.busUnit = busUnit;
	}

	public String getRotation() {
		return rotation;
	}

	public void setRotation(String rotation) {
		this.rotation = rotation;
	}
	
	@Override
	public String toString() {
		return "Rotation id=" + id + ", type=" + type + ", make=" + make + ", model=" + model + ", Serial=" + serialNumber + ", Designation=" + designation + ", BusUnit=" + busUnit + ", Rotation=" + rotation ;
	}
	
}
