package com.eoh.imp.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "Assets")
public class Asset implements Serializable {

	private static final long serialVersionUID = -3465813074586302847L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "type")
	private String type;

	@Column(name = "make")
	private String make;

	@Column(name = "model")
	private String model;

	@Column(name = "serialnumber")
	private String serialNumber;

	@Column(name = "dateofpurchase")
	@DateTimeFormat(pattern = "yyyy/mm/dd")
	private Date dateOfPurchase;

	@Column(name = "supplier")
	private String supplier;

	@Column(name = "comments")
	private String comments;

	@Column(name = "lifespan")
	@DateTimeFormat(pattern = "yyyy/mm/dd")
	private Date lifeSpan;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public Date getDateOfPurchase() {
		return dateOfPurchase;
	}

	public void setDateOfPurchase(Date dateOfPurchase) {
		this.dateOfPurchase = dateOfPurchase;
	}

	public String getSupplier() {
		return supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getLifeSpan() {
		return lifeSpan;
	}

	public void setLifeSpan(Date lifeSpan) {
		this.lifeSpan = lifeSpan;
	}

	@Override
	public String toString() {
		return "id=" + id + ", type=" + type + ", make=" + make + ", model=" + model + ", serialnumber=" + serialNumber
				+ ", dateofpurchase=" + dateOfPurchase + ", supplier=" + supplier + ", comments=" + comments
				+ ", lifespan=" + lifeSpan;
	}

}
