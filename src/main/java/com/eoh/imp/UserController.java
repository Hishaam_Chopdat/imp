package com.eoh.imp;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.eoh.imp.model.User;
import com.eoh.imp.service.AssetService;
import com.eoh.imp.service.UserService;

@Controller
@RequestMapping(value = "/users")
public class UserController {

	@Autowired
	public UserService userService;
	
	@Autowired
	public AssetService assetService;

	private int employeeId;
	
	@RequestMapping(value = "/Dashboard", method = RequestMethod.GET)
	public ModelAndView showDashboard(ModelAndView model) {
		boolean isValidUser = userService.validateCurrentUser();
		if (isValidUser) {
			model.addObject("currentUser", userService.getCurrentUser());
			model.addObject("assets", assetService.listAsset());
			model.addObject("rotations", assetService.listRotation());
			model.addObject("employees", userService.listEmployees());
			model.setViewName("ImpDashboard");
			return model;
		} else {
			model.setViewName("Login_Error");
			return model;
		}

	}

	@RequestMapping(value = "/ManageEmployees", method = RequestMethod.GET)
	public ModelAndView newEmployee(ModelAndView model) {
		boolean isValidUser = userService.validateCurrentUser();
		if (isValidUser) {
			List<String> roleInputList = userService.roleList();
			List<String> busInputList = userService.busUnitList();
			User user = new User();
			List<User> listEmployees = userService.listEmployees();
			model.addObject("currentUser", userService.getCurrentUser());
			model.addObject("busUnitList", busInputList);
			model.addObject("roleList", roleInputList);
			model.addObject("listEmployees", listEmployees);
			model.addObject("user", user);
			model.setViewName("ManageEmployees");
			return model;
		} else {
			model.setViewName("Login_Error");
			return model;
		}

	}

	@RequestMapping(value = "/addEmployee", method = RequestMethod.POST)
	public ModelAndView addEmployee(@ModelAttribute User user) {
		System.out.println(user);
		userService.register(user);
		return new ModelAndView("redirect:/users/ManageEmployees");
	}

	@RequestMapping(value = "/updateEmployee", method = RequestMethod.POST)
	public ModelAndView updateEmployee(@ModelAttribute User employeeform) {
		System.out.println(employeeform);
		employeeform.setEmplId(getEmployeeId());
		System.out.println(employeeform);
		userService.updateEmployee(employeeform);
		return new ModelAndView("redirect:/users/ManageEmployees");

	}

	@RequestMapping(value = "/deleteEmployee", method = RequestMethod.GET)
	public ModelAndView deleteEmployee(HttpServletRequest request) {
		int empId = Integer.parseInt(request.getParameter("id"));
		userService.removeEmployee(empId);
		return new ModelAndView("redirect:/users/ManageEmployees");
	}

	@RequestMapping(value = "/editEmployee", method = RequestMethod.GET)
	public ModelAndView editEmployee(HttpServletRequest request, ModelAndView model) {
		boolean isValidUser = userService.validateCurrentUser();
		if (isValidUser) {
			setEmployeeId(Integer.parseInt(request.getParameter("id")));
			User user = userService.getEmployeeById(getEmployeeId());
			List<String> roleInputList = userService.roleList();
			List<String> busInputList = userService.busUnitList();
			model.addObject("busUnitList", busInputList);
			model.addObject("roleList", roleInputList);
			model.addObject("currentUser", userService.getCurrentUser());
			model.addObject("employeeForm", user);
			model.setViewName("EmpEditForm");
			return model;
		} else {
			model.setViewName("Login_Error");
			return model;
		}
	}

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public ModelAndView addUser(ModelAndView model) {
		User user = new User();
		List<String> roleInputList = userService.roleList();
		List<String> busInputList = userService.busUnitList();
		model.addObject("busUnitList", busInputList);
		model.addObject("roleList", roleInputList);
		model.addObject("user", user);
		model.setViewName("Register");
		return model;
	}

	@RequestMapping(value = "/registerUser", method = RequestMethod.POST)
	public ModelAndView addUserProcess(@ModelAttribute User user) {
		System.out.println(user);
		userService.register(user);
		return new ModelAndView("redirect:/users/login");
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView showLogin(ModelAndView model) {
		model.addObject("loginUser", new User());
		model.setViewName("Login");
		return model;
	}

	@RequestMapping(value = "/LoginProcess", method = RequestMethod.POST)
	public ModelAndView loginProcess(@ModelAttribute User userlogin, final RedirectAttributes redirectAttributes) {
		System.out.println(userlogin);
		boolean isValidUser = userService.validateUser(userlogin);
		if (isValidUser) {
			return new ModelAndView("redirect:/users/Dashboard");
		} else {
			return new ModelAndView("redirect:/users/LoginError");
		}
	}

	@RequestMapping(value = "/LoginError", method = RequestMethod.GET)
	public ModelAndView showLoginError(ModelAndView model) {
		model.setViewName("Login_Error");
		return model;
	}

	@RequestMapping(value = "/Logout", method = RequestMethod.GET)
	public ModelAndView logUserOut(ModelAndView model) {
		userService.logout();
		return new ModelAndView("redirect:/users/login");
	}

	public void seUserService(UserService as) {
		this.userService = as;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

}
