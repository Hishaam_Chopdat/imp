package com.eoh.imp;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.eoh.imp.service.AssetService;
import com.eoh.imp.service.UserService;
import com.eoh.imp.model.Asset;
import com.eoh.imp.model.Rotation;

@Controller
@RequestMapping(value = "/assets")
public class AssetController {

	@Autowired
	private AssetService assetService;
	
	@Autowired
	private UserService userService;
	
	private int assetId;

	//////////////////////////////////////////MANAGE ASSETS/////////////////////////////////////////////////////////

	@RequestMapping(value = "/ManageAssets", method = RequestMethod.GET)
	public ModelAndView newAsset(ModelAndView model) {
		boolean isValidUser = userService.validateCurrentUser();
		if(isValidUser){
			Asset asset = new Asset();
			List<Asset> listAsset = assetService.listAsset();
			model.addObject("currentUser", userService.getCurrentUser());
			model.addObject("listAssets", listAsset);
			model.addObject("asset", asset);
			model.setViewName("ManageInventory");
			return model;
		}else{
			model.setViewName("Login_Error");
			return model;
		}
		
	}
	

	@RequestMapping(value = "/saveAsset", method = RequestMethod.POST)
	public ModelAndView saveAsset(@ModelAttribute Asset asset) {
		System.out.println(asset);
		assetService.addAsset(asset);
		return new ModelAndView("redirect:/assets/ManageAssets");

	}
	
	@RequestMapping(value = "/updateAsset", method = RequestMethod.POST)
	public ModelAndView updateAsset(@ModelAttribute Asset assetform) {
		System.out.println(assetform);
		assetform.setId(getAssetId());
		System.out.println(assetform);
		assetService.updateAsset(assetform);
		return new ModelAndView("redirect:/assets/ManageAssets");

	}

	@RequestMapping(value = "/deleteAsset", method = RequestMethod.GET)
	public ModelAndView deleteAsset(HttpServletRequest request) {
		int assetId = Integer.parseInt(request.getParameter("id"));
		Rotation rotation = assetService.getRotationById(assetId);
		rotation.setId(assetId);
		assetService.deleteRotation(rotation);
		assetService.removeAsset(assetId);
		return new ModelAndView("redirect:/assets/ManageAssets");
	}

	@RequestMapping(value = "/editAsset", method = RequestMethod.GET)
	public ModelAndView editAsset(HttpServletRequest request, ModelAndView model) {
		boolean isValidUser = userService.validateCurrentUser();
		if(isValidUser){
		setAssetId(Integer.parseInt(request.getParameter("id")));
		Asset asset = assetService.getAssetById(getAssetId());
		model.addObject("currentUser", userService.getCurrentUser());
		model.addObject("assetForm",asset);
		model.setViewName("EditForm");
		return model;
		}else{
			model.setViewName("Login_Error");
			return model;
		}
		
	}
	
	//////////////////////////////////////////MANAGE ROTATION/////////////////////////////////////////////////////////
	@RequestMapping(value = "/ManageRotation", method = RequestMethod.GET)
	public ModelAndView manageRotation(ModelAndView model) {
		boolean isValidUser = userService.validateCurrentUser();
		if(isValidUser){
			List<Rotation> listRotation = assetService.listRotation();
			model.addObject("currentUser", userService.getCurrentUser());
			model.addObject("listRotation", listRotation);
			model.setViewName("ManageRotation");
			return model;
		}else{
			model.setViewName("Login_Error");
			return model;
		}
		
	}
	
	@RequestMapping(value = "/updateRotation", method = RequestMethod.POST)
	public ModelAndView updateRotation(@ModelAttribute Rotation rotationform) {
		System.out.println(rotationform);
		rotationform.setId(getAssetId());
		System.out.println(rotationform);
		assetService.updateRotation(rotationform);
		return new ModelAndView("redirect:/assets/ManageRotation");
	}
	
	@RequestMapping(value = "/unassignRotation", method = RequestMethod.GET)
	public ModelAndView unassignRotation(HttpServletRequest request) {
		setAssetId(Integer.parseInt(request.getParameter("id")));
		Rotation rotation = assetService.getRotationById(assetId);
		System.out.println(rotation);
		rotation.setId(getAssetId());
		rotation.setDesignation(null);
		rotation.setBusUnit(null);
		rotation.setRotation("Unassigned");
		System.out.println(rotation);
		assetService.updateRotation(rotation);
		return new ModelAndView("redirect:/assets/ManageRotation");
	}
	
	@RequestMapping(value = "/editRotation", method = RequestMethod.GET)
	public ModelAndView editRotation(HttpServletRequest request, ModelAndView model) {
		boolean isValidUser = userService.validateCurrentUser();
		if(isValidUser){
		setAssetId(Integer.parseInt(request.getParameter("id")));
		Rotation rotation = assetService.getRotationById(getAssetId());
		System.out.println(rotation);
		List<String> userInputList = userService.userInputList();
		List<String> inputAssignmentList = assetService.inputAssignmentList();
		model.addObject("currentUser", userService.getCurrentUser());
		model.addObject("userInputList", userInputList);
		model.addObject("inputAssignmentList", inputAssignmentList);
		model.addObject("rotationForm",rotation);
		model.setViewName("RotationForm");
		return model;
		}else{
			model.setViewName("Login_Error");
			return model;
		}
	}
	
	//////////////////////////////////////////GETTERS & SETTERS/////////////////////////////////////////////////////////
	
	@RequestMapping(value = "/Logout", method = RequestMethod.GET)
	public ModelAndView logUserOut(ModelAndView model) {
		userService.logout();
		return new ModelAndView("redirect:/users/login");
	}

	public void setPersonService(AssetService as) {
		this.assetService = as;
	}

	public int getAssetId() {
		return assetId;
	}

	public void setAssetId(int assetId) {
		this.assetId = assetId;
	}

}
