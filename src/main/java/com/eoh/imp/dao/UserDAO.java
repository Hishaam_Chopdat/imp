package com.eoh.imp.dao;

import java.util.List;

import com.eoh.imp.model.Role;
import com.eoh.imp.model.User;

public interface UserDAO {

	void register(User user);
	
	public List<User> listEmployees();
	
	public User updateEmployee(User u);
	
	public void removeEmployee(int id);
	
	public User getEmployeeById(int id);

	public List<Role> listRole();

}
