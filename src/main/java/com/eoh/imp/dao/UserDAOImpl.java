package com.eoh.imp.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;

import com.eoh.imp.model.Role;
import com.eoh.imp.model.User;

@Repository
public class UserDAOImpl implements UserDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	@SuppressWarnings("unchecked")
	public List<User> listEmployees() {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(User.class);
		return (List<User>) criteria.list();

	}

	@Override
	public void removeEmployee(int id) {
		User u = (User) sessionFactory.getCurrentSession().load(User.class, id);
		if (null != u) {
			this.sessionFactory.getCurrentSession().delete(u);
		}
	}

	@Override
	public User updateEmployee(User u) {
		System.out.println(u);
		sessionFactory.getCurrentSession().update(u);
		return u;
	}

	@Override
	public void register(User user) {
		System.out.println(user);
		sessionFactory.getCurrentSession().saveOrUpdate(user);
	}


	@Override
	@SuppressWarnings("unchecked")
	public List<Role> listRole() {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Role.class);
		return (List<Role>) criteria.list();
	}

	@Override
	public User getEmployeeById(int id) {
		return (User) sessionFactory.getCurrentSession().get(User.class, id);
	}

}
