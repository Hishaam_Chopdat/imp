package com.eoh.imp.dao;

import java.util.List;

import com.eoh.imp.model.Asset;
import com.eoh.imp.model.Rotation;

public interface AssetDAO {
	
	public void addAsset(Asset a,Rotation r);
	public Asset updateAsset(Asset a);
	public List<Asset> listAsset();
	public List<Rotation> listRotation();
	public Asset getAssetById(int id);
	public void removeAsset(int id);
	public Rotation getRotationById(int id);
	public Rotation updateRotation(Rotation rotationform);
}
