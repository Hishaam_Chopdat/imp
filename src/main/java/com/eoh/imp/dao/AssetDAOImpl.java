package com.eoh.imp.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import com.eoh.imp.model.Asset;
import com.eoh.imp.model.Rotation;

@Repository
public class AssetDAOImpl implements AssetDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public void addAsset(Asset a,Rotation r) {
		System.out.println(a);
		System.out.println(r);
		sessionFactory.getCurrentSession().saveOrUpdate(a);
		sessionFactory.getCurrentSession().saveOrUpdate(r);
	}
	

	public Asset updateAsset(Asset asset) {
		System.out.println(asset);
		sessionFactory.getCurrentSession().update(asset);
		return asset;
	}

	@SuppressWarnings("unchecked")
	public List<Asset> listAsset() {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Asset.class);
		return (List<Asset>) criteria.list();
	}
	
	/////////////////////////////////////////////ROTATION///////////////////////////////////////
	@Override
	@SuppressWarnings("unchecked")
	public List<Rotation> listRotation() {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Rotation.class);
		return (List<Rotation>) criteria.list();
	}
	
	@Override
	public Rotation updateRotation(Rotation rotationform) {
		System.out.println(rotationform);
		sessionFactory.getCurrentSession().update(rotationform);
		return rotationform;
	}
	
	/////////////////////////////////////////////GET & SET///////////////////////////////////////

	public Asset getAssetById(int id) {
		return (Asset) sessionFactory.getCurrentSession().get(Asset.class, id);
	}

	public void removeAsset(int id) {
		Asset a = (Asset) sessionFactory.getCurrentSession().load(Asset.class, id);
		if (null != a) {
			this.sessionFactory.getCurrentSession().delete(a);
		}

	}
	
	@Override
	public Rotation getRotationById(int id) {
		return (Rotation) sessionFactory.getCurrentSession().get(Rotation.class, id);
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}


}
