package com.eoh.imp.service;

import java.util.List;

import com.eoh.imp.model.Asset;
import com.eoh.imp.model.Rotation;


public interface AssetService {

	public void addAsset(Asset a);
	public Asset updateAsset(Asset asset);
	public List<Asset> listAsset();
	public List<Rotation> listRotation();
	public Asset getAssetById(int id) ;
	public void removeAsset(int id);
	public Rotation getRotationById(int id);
	public Rotation updateRotation(Rotation rotationform);
	public void deleteRotation(Rotation rotation);
	public List<String> inputAssignmentList();

	
}
