package com.eoh.imp.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import com.eoh.imp.dao.AssetDAO;
import com.eoh.imp.model.Asset;
import com.eoh.imp.model.Rotation;

@Service
public class AssetServiceImpl implements AssetService {

	@Autowired
	private AssetDAO assetDAO;

	@Transactional
	public void addAsset(Asset a) {
		Rotation r = new Rotation();
		r.setId(a.getId());
		r.setType(a.getType());
		r.setMake(a.getMake());
		r.setModel(a.getModel());
		r.setSerialNumber(a.getSerialNumber());
		r.setRotation("Unassigned");
		System.out.println(a);
		System.out.println(r);
		this.assetDAO.addAsset(a, r);

	}

	@Transactional
	public Asset updateAsset(Asset asset) {
		System.out.println(asset);
		return this.assetDAO.updateAsset(asset);
	}

	@Transactional
	public List<Asset> listAsset() {
		return this.assetDAO.listAsset();
	}

	@Transactional
	public Asset getAssetById(int id) {
		return this.assetDAO.getAssetById(id);
	}

	@Transactional
	public void removeAsset(int id) {
		this.assetDAO.removeAsset(id);

	}


	/////////////////////////////////////////////////// ROTATION/////////////////////////////////////////
	@Override
	@Transactional
	public List<Rotation> listRotation() {
		return this.assetDAO.listRotation();
	}

	@Override
	@Transactional
	public Rotation getRotationById(int id) {
		return this.assetDAO.getRotationById(id);

	}
	
	@Override
	@Transactional
	public Rotation updateRotation(Rotation rotationform) {
		System.out.println(rotationform);
		return this.assetDAO.updateRotation(rotationform);
	}
	

	@Override
	@Transactional
	public void deleteRotation(Rotation rotation) {
		System.out.println(rotation);
		rotation.setRotation("Deleted");
		this.assetDAO.updateRotation(rotation);
	}
	
	@Override
	public List<String> inputAssignmentList() {
		List<String> al = new ArrayList<String>();
		al.add("Unassigned");
		al.add("Assigned");
		return al;
	}
	

	
	///////////////////////////////////////////////////GET & SET/////////////////////////////////////////


	public void setPersonDAO(AssetDAO assetDAO) {
		this.assetDAO = assetDAO;
	}

	

}
