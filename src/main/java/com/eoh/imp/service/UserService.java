package com.eoh.imp.service;

import java.util.List;

import com.eoh.imp.model.User;

public interface UserService {

	void register(User user);

	boolean validateUser(User login);
	
	public List<User> listEmployees();
	
	public User updateEmployee(User u);
	
	public void removeEmployee(int id);
	
	public User getEmployeeById(int id);
	
	public boolean validateCurrentUser();
	
	public List<String> roleList();
	
	public List<String> busUnitList();

	public User getCurrentUser();

	List<String> userInputList();
	
	public void logout();
	
}
