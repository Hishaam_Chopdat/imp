package com.eoh.imp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.eoh.imp.dao.UserDAO;
import com.eoh.imp.model.Role;
import com.eoh.imp.model.User;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserDAO userDao;
	User currentUser;

	@Override
	@Transactional
	public List<User> listEmployees() {
		return this.userDao.listEmployees();

	}

	@Override
	@Transactional
	public User updateEmployee(User user) {
		List<Role> roles = userDao.listRole();
		switch (user.getRole()) {
		case "Admin":
			user.setRole("" + roles.get(0).getRoleId());
			break;
		case "Manager":
			user.setRole("" + roles.get(1).getRoleId());
			break;
		case "Employee":
			user.setRole("" + roles.get(3).getRoleId());
			break;
		default:
			user.setRole("none");
		}
		;
		return userDao.updateEmployee(user);
	}

	@Override
	@Transactional
	public void removeEmployee(int id) {
		this.userDao.removeEmployee(id);
	}

	@Override
	@Transactional
	public User getEmployeeById(int id) {
		return this.userDao.getEmployeeById(id);
	}

	@Override
	public boolean validateCurrentUser() {
		if (getCurrentUser() != null) {
			System.out.println(getCurrentUser());
			return true;
		} else {
			return false;
		}
	}

	@Override
	@Transactional
	public void register(User user) {
		if (user != null) {
			List<Role> roles = userDao.listRole();
			switch (user.getRole()) {
			case "Admin":
				user.setRole("" + roles.get(0).getRoleId());
				break;
			case "Manager":
				user.setRole("" + roles.get(1).getRoleId());
				break;
			case "Employee":
				user.setRole("" + roles.get(3).getRoleId());
				break;
			default:
				user.setRole("none");
			}
			;
			System.out.println(user);
			userDao.register(user);
		}
	}

	@Override
	@Transactional
	public boolean validateUser(User userlogin) {
		System.out.println(userlogin);
		List<User> users = userDao.listEmployees();
		return validateLoginDetails(users, userlogin);
	}

	@Override
	@Transactional
	public List<String> roleList() {
		List<String> roleInputList = new ArrayList<String>();
		roleInputList.add("Select");
		roleInputList.add("Admin");
		roleInputList.add("Manager");
		roleInputList.add("Employee");
		return roleInputList;
	}

	@Override
	@Transactional
	public List<String> busUnitList() {
		List<String> busInputList = new ArrayList<String>();
		busInputList.add("Select");
		busInputList.add("EOH Digital");
		busInputList.add("Solutions");
		busInputList.add("Agency");
		busInputList.add("8bit Platoon");
		busInputList.add("Mobile");
		return busInputList;
	}

	@Override
	@Transactional
	public List<String> userInputList() {
		List<String> userInputList = new ArrayList<String>();
		List<User> users = this.userDao.listEmployees();
		for (User u : users) {
			userInputList.add("" + u.getFirstname() + " " + u.getLastname() + " | " + u.getEmail());
		}
		return userInputList;
	}

	public boolean validateLoginDetails(List<User> users, User userlogin) {
		String email = userlogin.getEmail();
		String password = userlogin.getPassword();
		boolean result = false;
		for (User u : users) {
			String role = u.getRole();
			if (u.getEmail().equals(email)) {
				if (u.getPassword().equals(password)) {
					switch (role) {
					case "ImpAdmin1":
						setCurrentUser(u);
						result = true;
						break;
					case "ImpManager1":
						setCurrentUser(u);
						result = true;
						break;
					case "ImpSuperAdmin1":
						setCurrentUser(u);
						result = true;
						break;
					}

				}
			}
		}
		return result;
	}

	@Override
	public void logout() {
		setCurrentUser(null);
	}

	@Override
	public User getCurrentUser() {
		return this.currentUser;
	}

	public void setCurrentUser(User currentUser) {
		this.currentUser = currentUser;
	}

}
