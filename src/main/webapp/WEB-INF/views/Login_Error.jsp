<html>
<head>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<!-- jQuery library -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>

	<div class="container">

		<!-------------------------------------- NAVBAR ---------------------------------------------->

			<nav class="navbar navbar-inverse" style="margin: 0px;">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
						aria-expanded="false">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="http://localhost:8080/Imp/">Imp</a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse"
					id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li><a href="#">Home<span class="sr-only">(current)</span></a></li>
						<li><a href="login.html">Login</a></li>
						<li><a href="register.html">Register</a></li>
						<li><a href="#">About</a></li>
					</ul>
				</div>
			</div>
		</nav>
		
		<!-------------------------------------- HERO JUMBOTRON ---------------------------------------------->
		<div class="jumbotron">
			<h2 style="text-align: center">LOGIN ERROR</h2>
			<p style="text-align: center">An Error Occured While Logging In</p>
			<p style="text-align: center"><a href="../users/login.html">Try Again</a></p>
			<br />
		</div>
	</div>

</body>
</html>
