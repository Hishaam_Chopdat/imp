<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<!------------------------------------------- RESOURCES -------------------------------------------------->



<!------------------------------------------- BOOTSTRAP CDN -------------------------------------------------->

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
	integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
	crossorigin="anonymous">

<script src="https://code.jquery.com/jquery-3.2.1.min.js"
	integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
	crossorigin="anonymous"></script>

<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
	integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
	crossorigin="anonymous"></script>


<!------------------------------------------- FORM VALIDATOR -------------------------------------------------->
<script src="<c:url value="/resources/js/LoginFormValidator.js" />" type="text/javascript"></script>

<!----------------------------------------- JQUERY VALIDATOR CDN'S ---------------------------------------------->
<script
	src='http://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js'></script>

<!----------------------------------------------- STYLE ----------------------------------------------------------->
<style>
.list-group-item {
	background-color: transparent;
	border: none
}
</style>

<!----------------------------------------------- TITLE ----------------------------------------------------------->
<title>Imp Login</title>
</head>

<body>

	<div class="container">

		<!-------------------------------------- NAVBAR ---------------------------------------------->

		<nav class="navbar navbar-inverse" style="margin: 0px;">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
					aria-expanded="false">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/Imp/">Imp</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a href="/Imp/">Home<span class="sr-only">(current)</span></a></li>
					<li><a href="/Imp/users/login">Login</a></li>
					<li><a href="/Imp/users/register">Register</a></li>
					<li><a href="#">About</a></li>
				</ul>
			</div>
		</div>
		</nav>

		<!-------------------------------------- HERO JUMBOTRON ---------------------------------------------->
		<div class="jumbotron" style="background-color: #5bc0de">
			<h2 style="text-align: center; margin: 0px; color: white">Login</h2>
		</div>

		<!--------------------------------------------- FORM -------------------------------------------------->
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4">

				<form:form action="LoginProcess" method="post"
					id="LoginForm" 
					commandName="loginUser">

					<div class="form-group">
						<label for="email">Email</label><br />
						<form:input path="email" placeholder="Enter Email"
							class="form-control" name="email" />
					</div>
					<div class="form-group">
						<label for="password">Password:</label><br />
						<form:input path="password" placeholder="Enter Password"
							class="form-control" type='password' name="password" />
					</div>

					<input type="submit" value="Login" class="btn btn-info">
				</form:form>
				<p style="text-align: center">
					<a href="register.html">Register</a>
				</p>
			</div>
			<div class="col-md-4"></div>

		</div>


		<!--------------------------------------------- FOOTER -------------------------------------------------->

		<div class="jumbotron"
			style="margin-top: 50px; background-color: #222;">
			<div class="row">
				<div class="col-sm-3">
					<div class="list-group">
						<button type="button" class="list-group-item">Cras justo
							odio</button>
						<button type="button" class="list-group-item">Dapibus ac
							facilisis in</button>
						<button type="button" class="list-group-item">Morbi leo
							risus</button>
						<button type="button" class="list-group-item">Porta ac
							consectetur ac</button>
						<button type="button" class="list-group-item">Vestibulum
							at eros</button>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="list-group">
						<button type="button" class="list-group-item">Cras justo
							odio</button>
						<button type="button" class="list-group-item">Dapibus ac
							facilisis in</button>
						<button type="button" class="list-group-item">Morbi leo
							risus</button>
						<button type="button" class="list-group-item">Porta ac
							consectetur ac</button>
						<button type="button" class="list-group-item">Vestibulum
							at eros</button>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="list-group">
						<button type="button" class="list-group-item">Cras justo
							odio</button>
						<button type="button" class="list-group-item">Dapibus ac
							facilisis in</button>
						<button type="button" class="list-group-item">Morbi leo
							risus</button>
						<button type="button" class="list-group-item">Porta ac
							consectetur ac</button>
						<button type="button" class="list-group-item">Vestibulum
							at eros</button>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="list-group">
						<button type="button" class="list-group-item">Cras justo
							odio</button>
						<button type="button" class="list-group-item">Dapibus ac
							facilisis in</button>
						<button type="button" class="list-group-item">Morbi leo
							risus</button>
						<button type="button" class="list-group-item">Porta ac
							consectetur ac</button>
						<button type="button" class="list-group-item">Vestibulum
							at eros</button>
					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- END CONTAINER -->

</body>
</html>