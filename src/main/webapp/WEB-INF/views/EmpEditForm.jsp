<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<!------------------------------------------- RESOURCES -------------------------------------------------->



<!------------------------------------------- BOOTSTRAP CDN -------------------------------------------------->

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
	integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
	crossorigin="anonymous">

<script src="https://code.jquery.com/jquery-3.2.1.min.js"
	integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
	crossorigin="anonymous"></script>

<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
	integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
	crossorigin="anonymous"></script>


<!------------------------------------------- FORM VALIDATOR -------------------------------------------------->
<script src="<c:url value="/resources/js/EmployeeFormValidator.js" />" type="text/javascript"></script>

<!----------------------------------------- JQUERY VALIDATOR CDN'S ---------------------------------------------->
<script
	src='http://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js'></script>

<!----------------------------------------------- STYLE ----------------------------------------------------------->
<style>
.list-group-item {
	background-color: transparent;
	border: none
}
</style>

<!----------------------------------------------- TITLE ----------------------------------------------------------->
<title>Imp Register</title>
</head>

<body>

	<div class="container">

		<!-------------------------------------- NAVBAR ---------------------------------------------->

		<nav class="navbar navbar-inverse" style="margin:0px;">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
					aria-expanded="false">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="http://localhost:8080/Imp/">Imp</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a href="/Imp/users/Dashboard">Dashboard</a></li>
					<li><a href="/Imp/assets/ManageAssets">Inventory</a></li>
					<li><a href="/Imp/users/ManageEmployees">Employees</a></li>
					<li><a href="/Imp/assets/ManageRotation">Rotation</a></li>
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false">Support<span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="#">User Guide</a></li>
							<li><a href="#">FAQ's</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="#">Contact Support</a></li>
						</ul></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false"><span class="glyphicon glyphicon-user"
							aria-hidden="true"></span> <c:out
								value="${currentUser.firstname}" /> | <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="#">Profile</a></li>
							<li><a href="#">Settings</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="/Imp/users/Logout">Logout</a></li>
						</ul></li>
				</ul>
			</div>
		</div>
		</nav>


		<!-------------------------------------- HERO JUMBOTRON ---------------------------------------------->
		<div class="jumbotron" style="background-color: #5bc0de">
			<h2 style="text-align: center; margin: 0px; color: white">Edit
				Employee</h2>
		</div>

		<!--------------------------------------------- FORM -------------------------------------------------->
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">

				<form:form action="updateEmployee" method="post"
					id="EmployeeForm"
					commandName="employeeForm">

					<div class="form-group">
						<label for="firstname">First Name</label><br />
						<form:input path="firstname" placeholder="Enter First Name"
							class="form-control" name="firstname" />
					</div>
					<div class="form-group">
						<label for="lastname">Last Name</label><br />
						<form:input path="lastname" placeholder="Enter Last Name"
							class="form-control" name="lastname" />
					</div>
					<div class="form-group">
						<label for="email">Email</label><br />
						<form:input path="email" placeholder="Enter Email"
							class="form-control" name="email" />
					</div>
					<div class="form-group">
						<label for="cell">Phone</label><br />
						<form:input path="cell" placeholder="Enter Phone Number"
							class="form-control" name="cell" />
					</div>
					<div class="form-group">
						<label for="addrress">Address</label><br />
						<form:input path="addrress" placeholder="Enter Address"
							class="form-control" type='text' name="addrress" />
					</div>
					<div class="form-group">
						<label for="role">User Type:</label><br />
						<form:select path="role" class="form-control">
							<form:options items="${roleList}" />
						</form:select>
					</div>
					<div class="form-group">
						<label for="busunit">Business Unit:</label><br />
						<form:select path="busUnit" class="form-control">
							<form:options items="${busUnitList}" />
						</form:select>
					</div>
					<div class="form-group">
						<label for="password">Password:</label><br />
						<form:input path="password" placeholder="Enter Password"
							class="form-control" type='text' name="password" />
					</div>

					<input type="submit" value="Update" class="btn btn-info">
				</form:form>

			</div>
			<div class="col-md-3"></div>

		</div>


		<!--------------------------------------------- FOOTER -------------------------------------------------->

		<div class="jumbotron"
			style="margin-top: 50px; background-color: #222;">
			<div class="row">
				<div class="col-sm-3">
					<div class="list-group">
						<button type="button" class="list-group-item">Cras justo
							odio</button>
						<button type="button" class="list-group-item">Dapibus ac
							facilisis in</button>
						<button type="button" class="list-group-item">Morbi leo
							risus</button>
						<button type="button" class="list-group-item">Porta ac
							consectetur ac</button>
						<button type="button" class="list-group-item">Vestibulum
							at eros</button>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="list-group">
						<button type="button" class="list-group-item">Cras justo
							odio</button>
						<button type="button" class="list-group-item">Dapibus ac
							facilisis in</button>
						<button type="button" class="list-group-item">Morbi leo
							risus</button>
						<button type="button" class="list-group-item">Porta ac
							consectetur ac</button>
						<button type="button" class="list-group-item">Vestibulum
							at eros</button>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="list-group">
						<button type="button" class="list-group-item">Cras justo
							odio</button>
						<button type="button" class="list-group-item">Dapibus ac
							facilisis in</button>
						<button type="button" class="list-group-item">Morbi leo
							risus</button>
						<button type="button" class="list-group-item">Porta ac
							consectetur ac</button>
						<button type="button" class="list-group-item">Vestibulum
							at eros</button>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="list-group">
						<button type="button" class="list-group-item">Cras justo
							odio</button>
						<button type="button" class="list-group-item">Dapibus ac
							facilisis in</button>
						<button type="button" class="list-group-item">Morbi leo
							risus</button>
						<button type="button" class="list-group-item">Porta ac
							consectetur ac</button>
						<button type="button" class="list-group-item">Vestibulum
							at eros</button>
					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- END CONTAINER -->

</body>
</html>