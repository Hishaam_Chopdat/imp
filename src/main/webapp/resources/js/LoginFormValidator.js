$(document).ready(function() {
	$('#LoginForm').bootstrapValidator({
		// To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
		feedbackIcons : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			email : {
				validators : {
					stringLength : {
						min : 3,
					},
					notEmpty : {
						message : 'Required : min 3 - max 255 char'
					}
				}
			},
			password : {
				validators : {
					stringLength : {
						min : 3,
					},
					notEmpty : {
						message : 'Required : min 3 - max 255 char'
					}
				}
			},
		}
	}).on('success.form.bv', function(e) {
		$('#success_message').slideDown({
			opacity : "show"
		}, "slow") // Do something ...
		$('#reg_form').data('bootstrapValidator').resetForm();

		// Prevent form submission
		e.preventDefault();

		// Get the form instance
		var $form = $(e.target);

		// Get the BootstrapValidator instance
		var bv = $form.data('bootstrapValidator');

		// Use Ajax to submit form data
		$.post($form.attr('action'), $form.serialize(), function(result) {
			console.log(result);
		}, 'json');
	});
});