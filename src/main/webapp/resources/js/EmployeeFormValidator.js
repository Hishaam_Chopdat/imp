$(document).ready(function() {
	$('#EmployeeForm').bootstrapValidator({
		// To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
		feedbackIcons : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			firstname : {
				validators : {
					stringLength : {
						min : 3,
					},
					notEmpty : {
						message : 'Required : min 3 - max 255 char'
					}
				}
			},
			lastname : {
				validators : {
					stringLength : {
						min : 3,
					},
					notEmpty : {
						message : 'Required : min 3 - max 255 char'
					}
				}
			},
			email : {
				validators : {
					stringLength : {
						min : 3,
					},
					notEmpty : {
						message : 'Please input valid email address'
					}
				}
			},

			cell : {
				validators : {
					stringLength : {
						min : 3,
					},
					notEmpty : {
						message : 'Please input valid cell number'
					}
				}
			},


		}
	}).on('success.form.bv', function(e) {
		$('#success_message').slideDown({
			opacity : "show"
		}, "slow") // Do something ...
		$('#reg_form').data('bootstrapValidator').resetForm();

		// Prevent form submission
		e.preventDefault();

		// Get the form instance
		var $form = $(e.target);

		// Get the BootstrapValidator instance
		var bv = $form.data('bootstrapValidator');

		// Use Ajax to submit form data
		$.post($form.attr('action'), $form.serialize(), function(result) {
			console.log(result);
		}, 'json');
	});
});