<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<!------------------------------------------- RESOURCES -------------------------------------------------->

<link type="text/css" href="resources/css/ManageInventory.css"
	rel="stylesheet" />


<!------------------------------------------- BOOTSTRAP CDN -------------------------------------------------->

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
	integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
	crossorigin="anonymous">

<script src="https://code.jquery.com/jquery-3.2.1.min.js"
	integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
	crossorigin="anonymous"></script>

<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
	integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
	crossorigin="anonymous"></script>

<!------------------------------------------- FORM VALIDATOR -------------------------------------------------->
<script src="resources/js/AssetFormValidator.js" type="text/javascript"></script>

<!----------------------------------------- JQUERY VALIDATOR CDN'S ---------------------------------------------->
<script
	src='http://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js'></script>

<!----------------------------------------------- STYLE ----------------------------------------------------------->
<style>
.list-group-item {
	background-color: transparent;
	border: none
}

div.searchCol {
	padding: 0px;
}

p#searchHead {
	font-weight: bold;
	margin-bottom: 0px;
}

input.searchInput {
	margin-bottom: 10px;
}
.modal-header,.modal-footer {
    background-color: #296193;
    color: #fff;
}
</style>

<title>Imp Manage Rotation</title>
</head>

<body>

	<div class="container">

		<!-------------------------------------- NAVBAR ---------------------------------------------->

		<nav class="navbar navbar-inverse" style="margin:0px;">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
					aria-expanded="false">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="http://localhost:8080/Imp/">Imp</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a href="/Imp/users/Dashboard">Dashboard</a></li>
					<li><a href="/Imp/assets/ManageAssets">Inventory</a></li>
					<li><a href="/Imp/users/ManageEmployees">Employees</a></li>
					<li class="active"><a href="/Imp/assets/ManageRotation">Rotation<span
							class="sr-only">(current)</span></a></li>
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false">Support<span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="#">User Guide</a></li>
							<li><a href="#">FAQ's</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="#">Contact Support</a></li>
						</ul></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false"><span class="glyphicon glyphicon-user"
							aria-hidden="true"></span> <c:out
								value="${currentUser.firstname}" /> | <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="#">Profile</a></li>
							<li><a href="#">Settings</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="/Imp/users/Logout">Logout</a></li>
						</ul></li>
				</ul>
			</div>
		</div>
		</nav>

		<!-------------------------------------- HERO JUMBOTRON ---------------------------------------------->
		<div class="jumbotron">
			<h2 style="text-align: center; margin: 0px;">Manage Rotation</h2>
			<p style="text-align: center">View . Add . Edit . Unassign Your
				Rotation</p>
		</div>


	</div>
	<!-- END CONTAINER -->
	<!-------------------------------------- ASSETS TABLE ---------------------------------------------->
	<div class="container">
		<table class="table table-hover search-table">
			<thead>
				<tr>
					<th>Asset Id</th>
					<th>Asset Type</th>
					<th>Asset Make</th>
					<th>Asset Model</th>
					<th>Serial Number</th>
					<th>Designation</th>
					<th>busUnit</th>
					<th>rotation</th>
					<th>Actions</th>
				</tr>
			</thead>

			<c:forEach var="rotation" items="${listRotation}">
				<tr>
					<td>${rotation.id}</td>
					<td>${rotation.type}</td>
					<td>${rotation.make}</td>
					<td>${rotation.model}</td>
					<td>${rotation.serialNumber}</td>
					<td>${rotation.designation}</td>
					<td>${rotation.busUnit}</td>
					<td>${rotation.rotation}</td>
					<td><a href="editRotation?id=${rotation.id}"
						class="btn btn-info col-lg-6"><span
							class="glyphicon glyphicon-user" aria-hidden="true"></span></a> <a
						href="unassignRotation?id=${rotation.id}"
						class="btn btn-danger col-lg-6"><span
							class="glyphicon glyphicon-transfer" aria-hidden="true"></span> </a></td>
				</tr>
			</c:forEach>

		</table>
	</div>



	<!--------------------------------------------- FOOTER -------------------------------------------------->
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="jumbotron"
					style="margin-top: 50px; background-color: #222;">
					<div class="row">
						<div class="col-sm-3">
							<div class="list-group">
								<button type="button" class="list-group-item">Cras
									justo odio</button>
								<button type="button" class="list-group-item">Dapibus
									ac facilisis in</button>
								<button type="button" class="list-group-item">Morbi leo
									risus</button>
								<button type="button" class="list-group-item">Porta ac
									consectetur ac</button>
								<button type="button" class="list-group-item">Vestibulum
									at eros</button>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="list-group">
								<button type="button" class="list-group-item">Cras
									justo odio</button>
								<button type="button" class="list-group-item">Dapibus
									ac facilisis in</button>
								<button type="button" class="list-group-item">Morbi leo
									risus</button>
								<button type="button" class="list-group-item">Porta ac
									consectetur ac</button>
								<button type="button" class="list-group-item">Vestibulum
									at eros</button>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="list-group">
								<button type="button" class="list-group-item">Cras
									justo odio</button>
								<button type="button" class="list-group-item">Dapibus
									ac facilisis in</button>
								<button type="button" class="list-group-item">Morbi leo
									risus</button>
								<button type="button" class="list-group-item">Porta ac
									consectetur ac</button>
								<button type="button" class="list-group-item">Vestibulum
									at eros</button>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="list-group">
								<button type="button" class="list-group-item">Cras
									justo odio</button>
								<button type="button" class="list-group-item">Dapibus
									ac facilisis in</button>
								<button type="button" class="list-group-item">Morbi leo
									risus</button>
								<button type="button" class="list-group-item">Porta ac
									consectetur ac</button>
								<button type="button" class="list-group-item">Vestibulum
									at eros</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!----------------------------------------------- SEARCH JQUERY ----------------------------------------------------------->

	<script type="text/javascript">
		(function($) {
			$.fn.tableSearch = function(options) {
				if (!$(this).is('table')) {
					return;
				}
				var tableObj = $(this), searchText = (options.searchText) ? options.searchText
						: 'Search: ', searchPlaceHolder = (options.searchPlaceHolder) ? options.searchPlaceHolder
						: '', divObj = $('<div class="col-sm-3 searchCol"><p id="searchHead">'
						+ searchText + '</p></div>'), inputObj = $('<input type="text" placeholder="'+searchPlaceHolder+'" class="form-control searchInput"/>'), caseSensitive = (options.caseSensitive === true) ? true
						: false, searchFieldVal = '', pattern = '';
				inputObj.off('keyup').on(
						'keyup',
						function() {
							searchFieldVal = $(this).val();
							pattern = (caseSensitive) ? RegExp(searchFieldVal)
									: RegExp(searchFieldVal, 'i');
							tableObj.find('tbody tr').hide().each(function() {
								var currentRow = $(this);
								currentRow.find('td').each(function() {
									if (pattern.test($(this).html())) {
										currentRow.show();
										return false;
									}
								});
							});
						});
				tableObj.before(divObj.append(inputObj));
				return tableObj;
			}
		}(jQuery));
	</script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('table.search-table').tableSearch({
				searchText : 'Search ',
				searchPlaceHolder : 'Input Value'
			});
		});
	</script>


</body>
</html>