<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<!------------------------------------------- RESOURCES -------------------------------------------------->


<!------------------------------------------- BOOTSTRAP CDN -------------------------------------------------->

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
	integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
	crossorigin="anonymous">

<script src="https://code.jquery.com/jquery-3.2.1.min.js"
	integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
	crossorigin="anonymous"></script>

<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
	integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
	crossorigin="anonymous"></script>

<!------------------------------------------- FORM VALIDATOR -------------------------------------------------->
<script src="<c:url value="/resources/js/AssetFormValidator.js" />" type="text/javascript"></script>
<!------------------------------------------- DATEPICKER CDN'S -------------------------------------------------->
<link rel="stylesheet"
	href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
<script
	src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<!----------------------------------------- JQUERY VALIDATOR CDN'S ---------------------------------------------->
<script
	src='http://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js'></script>
<!----------------------------------------------- STYLE ----------------------------------------------------------->
<style>
.list-group-item {
	background-color: transparent;
	border: none
}
</style>
<!----------------------------------------------- TITLE ----------------------------------------------------------->
<title>Imp Edit Asset</title>
</head>

<body>

	<div class="container">

		<!-------------------------------------- NAVBAR ---------------------------------------------->

		<nav class="navbar navbar-inverse" style="margin:0px;">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
					aria-expanded="false">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="http://localhost:8080/Imp/">Imp</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a href="/Imp/users/Dashboard">Dashboard</a></li>
					<li><a href="/Imp/assets/ManageAssets">Inventory</a></li>
					<li><a href="/Imp/users/ManageEmployees">Employees</a></li>
					<li><a href="/Imp/assets/ManageRotation">Rotation</a></li>
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false">Support<span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="#">User Guide</a></li>
							<li><a href="#">FAQ's</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="#">Contact Support</a></li>
						</ul></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false"><span class="glyphicon glyphicon-user"
							aria-hidden="true"></span> <c:out
								value="${currentUser.firstname}" /> | <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="#">Profile</a></li>
							<li><a href="#">Settings</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="/Imp/users/Logout">Logout</a></li>
						</ul></li>
				</ul>
			</div>
		</div>
		</nav>

		<!-------------------------------------- HERO JUMBOTRON ---------------------------------------------->
		<div class="jumbotron" style="background-color: #5bc0de">
			<h2 style="text-align: center; margin: 0px; color: white">Edit
				Asset</h2>
		</div>

		<!--------------------------------------------- FORM -------------------------------------------------->

		<form:form action="updateAsset" method="post" commandName="assetForm">

			<div class="form-group">
				<label for="id">Asset ID:</label>
				<form:input path="id" placeholder="ID Autogenerated" disabled="true"
					class="form-control" />
			</div>
			<div class="form-group">
				<label for="type">Asset Type:</label><br />
				<form:input path="type" placeholder="Enter Asset's Type"
					class="form-control" name="type" />
			</div>
			<div class="form-group">
				<label for="make">Asset Make:</label><br />
				<form:input path="make" placeholder="Enter Asset's Make"
					class="form-control" name="make" />
			</div>
			<div class="form-group">
				<label for="model">Asset Model:</label><br />
				<form:input path="model" placeholder="Enter Model's Name"
					class="form-control" name="model" />
			</div>
			<div class="form-group">
				<label for="serial">Serial Number:</label><br />
				<form:input path="serialNumber" placeholder="Enter Serial Number"
					class="form-control" name="serialnumber" />
			</div>
			<div class="form-group">
				<label for="dop">Date Of Purchase:</label><br />
				<div class="input-group date" id="datepicker1">
					<form:input path="dateOfPurchase" placeholder="YYYY/MM/DD"
						class="form-control" type='text' name="dop" />
					<span class="input-group-addon"> <span
						class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
			<div class="form-group">
				<label for="supplier">Supplier:</label><br />
				<form:input path="supplier" placeholder="Enter Supplier Name"
					class="form-control" name="supplier" />
			</div>
			<div class="form-group">
				<label for="comments">Comments:</label><br />
				<form:input path="comments" placeholder="Enter Comments"
					class="form-control" name="comments" />
			</div>
			<div class="form-group">
				<label for="life">Life Span:</label><br />
				<div class="input-group date" id="datepicker2">
					<form:input path="lifeSpan" placeholder="YYYY/MM/DD"
						class="form-control" type='text' name="lifespan" />
					<span class="input-group-addon"> <span
						class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>

			</div>
			<input type="submit" value="Update" class="btn btn-info">
		</form:form>

		<script>
			$(function() {
				$('#datepicker1').datepicker({
					format : 'yyyy/mm/dd'
				});
			});
			$(function() {
				$('#datepicker2').datepicker({
					format : 'yyyy/mm/dd'
				});
			});
		</script>

		<!--------------------------------------------- FOOTER -------------------------------------------------->

		<div class="jumbotron"
			style="margin-top: 50px; background-color: #222;">
			<div class="row">
				<div class="col-sm-3">
					<div class="list-group">
						<button type="button" class="list-group-item">Cras justo
							odio</button>
						<button type="button" class="list-group-item">Dapibus ac
							facilisis in</button>
						<button type="button" class="list-group-item">Morbi leo
							risus</button>
						<button type="button" class="list-group-item">Porta ac
							consectetur ac</button>
						<button type="button" class="list-group-item">Vestibulum
							at eros</button>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="list-group">
						<button type="button" class="list-group-item">Cras justo
							odio</button>
						<button type="button" class="list-group-item">Dapibus ac
							facilisis in</button>
						<button type="button" class="list-group-item">Morbi leo
							risus</button>
						<button type="button" class="list-group-item">Porta ac
							consectetur ac</button>
						<button type="button" class="list-group-item">Vestibulum
							at eros</button>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="list-group">
						<button type="button" class="list-group-item">Cras justo
							odio</button>
						<button type="button" class="list-group-item">Dapibus ac
							facilisis in</button>
						<button type="button" class="list-group-item">Morbi leo
							risus</button>
						<button type="button" class="list-group-item">Porta ac
							consectetur ac</button>
						<button type="button" class="list-group-item">Vestibulum
							at eros</button>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="list-group">
						<button type="button" class="list-group-item">Cras justo
							odio</button>
						<button type="button" class="list-group-item">Dapibus ac
							facilisis in</button>
						<button type="button" class="list-group-item">Morbi leo
							risus</button>
						<button type="button" class="list-group-item">Porta ac
							consectetur ac</button>
						<button type="button" class="list-group-item">Vestibulum
							at eros</button>
					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- END CONTAINER -->

</body>
</html>