<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<!------------------------------------------- RESOURCES -------------------------------------------------->

<link type="text/css" href="resources/css/ManageInventory.css"
	rel="stylesheet" />


<!------------------------------------------- BOOTSTRAP CDN -------------------------------------------------->

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
	integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
	crossorigin="anonymous">

<script src="https://code.jquery.com/jquery-3.2.1.min.js"
	integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
	crossorigin="anonymous"></script>

<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
	integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
	crossorigin="anonymous"></script>


<!----------------------------------------------- STYLE ----------------------------------------------------------->
<style>
.list-group-item {
	background-color: transparent;
	border: none
}
#buttonJumbotron .glyphicon{
	color:#00ff23;
	font-size: 115px;
}
</style>

<title>Imp Home</title>

</head>
<body>

	<div class="container">

		<!-------------------------------------- NAVBAR ---------------------------------------------->

		<nav class="navbar navbar-inverse" style="margin: 0px;">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
					aria-expanded="false">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="http://localhost:8080/Imp/">Imp</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li class="active"><a href="/Imp/users/Dashboard">Dashboard</a></li>
					<li><a href="/Imp/assets/ManageAssets">Inventory</a></li>
					<li><a href="#">Employees</a></li>
					<li><a href="/Imp/assets/ManageRotation">Rotation</a></li>
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false">Support<span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="#">User Guide</a></li>
							<li><a href="#">FAQ's</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="#">Contact Support</a></li>
						</ul></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false"><span class="glyphicon glyphicon-user"
							aria-hidden="true"></span> <c:out
								value="${currentUser.firstname}" /> | <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="#">Profile</a></li>
							<li><a href="#">Settings</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="/Imp/users/Logout">Logout</a></li>
						</ul></li>
				</ul>
			</div>
		</div>
		</nav>

		<!-------------------------------------- HERO JUMBOTRON ---------------------------------------------->
		<div class="jumbotron" id="buttonJumbotron">
			<h2 style="text-align: center">Welcome to Imp</h2>
			<h3 style="text-align: center">Asset Tracking at it's finest.</h3>
			<br />
			<p style="text-align: center">
				<a href="/Imp/assets/ManageAssets" class="btn btn-primary btn-lg"><span
					class="glyphicon glyphicon-hdd" aria-hidden="true"></span><br><br>Manage
					Inventory <span class="badge"><c:out
							value="${assets.size()}" /></span> </a> <a
					href="/Imp/assets/ManageRotation" class="btn btn-primary btn-lg"><span
					class="glyphicon glyphicon-transfer" aria-hidden="true"></span><br><br>Manage
					Rotation <span class="badge"><c:out
							value="${rotations.size()}" /></span> </a> <a
					href="/Imp/users/ManageEmployees" class="btn btn-primary btn-lg"><span
					class="glyphicon glyphicon-user" aria-hidden="true"></span><br><br>Manage
					Employees <span class="badge"><c:out
							value="${employees.size()}" /></span> </a>
			</p>
		</div>

		<!--------------------------------------------- FOOTER -------------------------------------------------->
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="jumbotron"
						style="margin-top: 50px; background-color: #222;">
						<div class="row">
							<div class="col-sm-3">
								<div class="list-group">
									<button type="button" class="list-group-item">Cras
										justo odio</button>
									<button type="button" class="list-group-item">Dapibus
										ac facilisis in</button>
									<button type="button" class="list-group-item">Morbi
										leo risus</button>
									<button type="button" class="list-group-item">Porta ac
										consectetur ac</button>
									<button type="button" class="list-group-item">Vestibulum
										at eros</button>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="list-group">
									<button type="button" class="list-group-item">Cras
										justo odio</button>
									<button type="button" class="list-group-item">Dapibus
										ac facilisis in</button>
									<button type="button" class="list-group-item">Morbi
										leo risus</button>
									<button type="button" class="list-group-item">Porta ac
										consectetur ac</button>
									<button type="button" class="list-group-item">Vestibulum
										at eros</button>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="list-group">
									<button type="button" class="list-group-item">Cras
										justo odio</button>
									<button type="button" class="list-group-item">Dapibus
										ac facilisis in</button>
									<button type="button" class="list-group-item">Morbi
										leo risus</button>
									<button type="button" class="list-group-item">Porta ac
										consectetur ac</button>
									<button type="button" class="list-group-item">Vestibulum
										at eros</button>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="list-group">
									<button type="button" class="list-group-item">Cras
										justo odio</button>
									<button type="button" class="list-group-item">Dapibus
										ac facilisis in</button>
									<button type="button" class="list-group-item">Morbi
										leo risus</button>
									<button type="button" class="list-group-item">Porta ac
										consectetur ac</button>
									<button type="button" class="list-group-item">Vestibulum
										at eros</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>

</body>
</html>
